import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:login_test/authentication/authentication_bloc.dart';
import 'package:login_test/authentication/authentication_event.dart';
import 'package:login_test/authentication/authentication_state.dart';
import 'package:login_test/model/User.dart';
import 'package:login_test/model/usecase/get_user_use_case.dart';
import 'package:login_test/model/usecase/logout_use_case.dart';
import 'package:login_test/model/usecase/result.dart';

void main() {
  User user = User(email: "a@b.com");
  Success success = Success(user);
  Failure failure = Failure("Failed test");

  blocTest<AuthenticationBloc, AuthenticationState>(
      "Initial - user exists - Authenticated",
      build: () {
        LogoutUsecaseMock logoutUsecase = LogoutUsecaseMock();
        GetUserUsecaseMock getUserUsecase = GetUserUsecaseMock();
        getUserUsecase.result = success;
        return AuthenticationBloc(getUserUsecase, logoutUsecase);
      },
      act: (bloc) => bloc.add(Initial()),
      expect: () => <AuthenticationState>[Authenticated(user)]);

  blocTest<AuthenticationBloc, AuthenticationState>(
      "Initial - user does not exist - Unauthenticated",
      build: () {
        LogoutUsecaseMock logoutUsecase = LogoutUsecaseMock();
        GetUserUsecaseMock getUserUsecase = GetUserUsecaseMock();
        getUserUsecase.result = failure;
        return AuthenticationBloc(getUserUsecase, logoutUsecase);
      },
      act: (bloc) => bloc.add(Initial()),
      expect: () => <AuthenticationState>[Unauthenticated()]);

  blocTest<AuthenticationBloc, AuthenticationState>(
      "LoggedIn - user exists - Authenticated",
      build: () {
        LogoutUsecaseMock logoutUsecase = LogoutUsecaseMock();
        GetUserUsecaseMock getUserUsecase = GetUserUsecaseMock();
        getUserUsecase.result = success;
        return AuthenticationBloc(getUserUsecase, logoutUsecase);
      },
      act: (bloc) => bloc.add(LoggedIn()),
      expect: () => <AuthenticationState>[Authenticated(user)]);

  blocTest<AuthenticationBloc, AuthenticationState>(
      "LoggedIn - user does not exist - Unauthenticated",
      build: () {
        LogoutUsecaseMock logoutUsecase = LogoutUsecaseMock();
        GetUserUsecaseMock getUserUsecase = GetUserUsecaseMock();
        getUserUsecase.result = failure;
        return AuthenticationBloc(getUserUsecase, logoutUsecase);
      },
      act: (bloc) => bloc.add(LoggedIn()),
      expect: () => <AuthenticationState>[Unauthenticated()]);

  blocTest<AuthenticationBloc, AuthenticationState>("LoggedOut",
      build: () {
        LogoutUsecaseMock logoutUsecase = LogoutUsecaseMock();
        GetUserUsecaseMock getUserUsecase = GetUserUsecaseMock();
        return AuthenticationBloc(getUserUsecase, logoutUsecase);
      },
      act: (bloc) => bloc.add(LoggedOut()),
      expect: () => <AuthenticationState>[Unauthenticated()]);
}

class LogoutUsecaseMock implements LogoutUsecase {
  @override
  Future<void> execute() {
    return Future.value();
  }
}

class GetUserUsecaseMock implements GetUserUsecase {
  Result result = Failure("Missing value");

  @override
  Result execute() {
    return result;
  }
}
