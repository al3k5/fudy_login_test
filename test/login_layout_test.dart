import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations_en.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:login_test/login/login_layout.dart';
import 'package:login_test/login/login_state.dart';

void main() {
  testWidgets("Login screen - errors shown", (WidgetTester tester) async {
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    LoginState state = LoginState()
        .copyWith(isEmailValid: false, isPasswordValid: false);
    Widget widgetUnderTest = MaterialApp(
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      home: LoginLayout(
          state: state,
          emailController: emailController,
          passwordController: passwordController),
    );

    await tester.pumpWidget(widgetUnderTest);

    final emailErrorFinder =
        find.textContaining(AppLocalizationsEn().loginEmailError);
    expect(emailErrorFinder, findsOneWidget);
    final passwordErrorFinder =
        find.textContaining(AppLocalizationsEn().loginPasswordError);
    expect(passwordErrorFinder, findsOneWidget);
  });
}
