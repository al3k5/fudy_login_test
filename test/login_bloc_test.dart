import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:login_test/login/login_bloc.dart';
import 'package:login_test/login/login_event.dart';
import 'package:login_test/login/login_state.dart';
import 'package:login_test/model/usecase/login_use_case.dart';

void main() {
  late LoginBloc loginBloc;
  setUp(() {
    loginBloc = LoginBloc(loginUserUseCase: LoginUseCaseMock());
  });

  String validEmail = "a@b.com";
  String validPassword = "qwerty123";
  String invalidEmail = "akjsdn";
  String invalidPassword = "ndsjak";
  LoginState initial = LoginState();

  blocTest<LoginBloc, LoginState>("Login clicked - success",
      build: () => loginBloc,
      act: (bloc) => bloc.add(LoginClicked(validEmail, validPassword)),
      expect: () => <LoginState>[
            initial,
            initial.copyWith(
                isEmailValid: true, isPasswordValid: true, isLoginSuccess: true)
          ]);

  blocTest<LoginBloc, LoginState>("Login clicked - nothing happens",
      build: () => loginBloc,
      seed: () => initial.copyWith(isRegisterSuccess: true),
      act: (bloc) => bloc.add(LoginClicked(validEmail, validPassword)),
      expect: () => <LoginState>[]);

  blocTest<LoginBloc, LoginState>("Login clicked - email invalid",
      build: () => loginBloc,
      seed: () => initial,
      act: (bloc) => bloc.add(LoginClicked(invalidEmail, validPassword)),
      expect: () => <LoginState>[initial.copyWith(isEmailValid: false)]);

  blocTest<LoginBloc, LoginState>("Login clicked - password invalid",
      build: () => loginBloc,
      seed: () => initial,
      act: (bloc) => bloc.add(LoginClicked(validEmail, invalidPassword)),
      expect: () => <LoginState>[initial.copyWith(isPasswordValid: false)]);

  blocTest<LoginBloc, LoginState>("Login clicked - password & email invalid",
      build: () => loginBloc,
      seed: () => initial,
      act: (bloc) => bloc.add(LoginClicked(invalidEmail, invalidPassword)),
      expect: () => <LoginState>[
            initial.copyWith(isPasswordValid: false, isEmailValid: false)
          ]);

  blocTest<LoginBloc, LoginState>(
      "Login clicked - invalid fields fixed, login success",
      build: () => loginBloc,
      seed: () => initial.copyWith(isEmailValid: false, isPasswordValid: false),
      act: (bloc) => bloc.add(LoginClicked(validEmail, validPassword)),
      expect: () => <LoginState>[
            initial.copyWith(isPasswordValid: true, isEmailValid: true),
            initial.copyWith(
                isPasswordValid: true, isEmailValid: true, isLoginSuccess: true)
          ]);

  blocTest<LoginBloc, LoginState>("Register clicked - nothing happens",
      build: () => loginBloc,
      seed: () => initial.copyWith(isRegisterSuccess: true),
      act: (bloc) => bloc.add(RegisterClicked(validEmail, validPassword)),
      expect: () => <LoginState>[]);

  blocTest<LoginBloc, LoginState>("Register clicked - email invalid",
      build: () => loginBloc,
      seed: () => initial,
      act: (bloc) => bloc.add(RegisterClicked(invalidEmail, validPassword)),
      expect: () => <LoginState>[initial.copyWith(isEmailValid: false)]);

  blocTest<LoginBloc, LoginState>("Register clicked - password invalid",
      build: () => loginBloc,
      seed: () => initial,
      act: (bloc) => bloc.add(RegisterClicked(validEmail, invalidPassword)),
      expect: () => <LoginState>[initial.copyWith(isPasswordValid: false)]);

  blocTest<LoginBloc, LoginState>("Register clicked - password & email invalid",
      build: () => loginBloc,
      seed: () => initial,
      act: (bloc) => bloc.add(RegisterClicked(invalidEmail, invalidPassword)),
      expect: () => <LoginState>[
            initial.copyWith(isPasswordValid: false, isEmailValid: false)
          ]);

  blocTest<LoginBloc, LoginState>(
      "Register clicked - invalid fields fixed, registration success",
      build: () => loginBloc,
      seed: () => initial.copyWith(isEmailValid: false, isPasswordValid: false),
      act: (bloc) => bloc.add(RegisterClicked(validEmail, validPassword)),
      expect: () => <LoginState>[
            initial.copyWith(isPasswordValid: true, isEmailValid: true),
            initial.copyWith(
                isPasswordValid: true,
                isEmailValid: true,
                isRegisterSuccess: true),
            initial.copyWith(
                isPasswordValid: true,
                isEmailValid: true,
                isRegisterSuccess: false)
          ]);

  blocTest<LoginBloc, LoginState>(
      "Form submitted - invalid fields fixed, registration success",
      build: () => loginBloc,
      seed: () => initial.copyWith(
          isEmailValid: false,
          isPasswordValid: false,
          isLoginTogglePosition: false),
      act: (bloc) => bloc.add(FormSubmitted(validEmail, validPassword)),
      expect: () => <LoginState>[
            initial.copyWith(
                isPasswordValid: true,
                isEmailValid: true,
                isLoginTogglePosition: false),
            initial.copyWith(
                isLoginTogglePosition: false,
                isPasswordValid: true,
                isEmailValid: true,
                isRegisterSuccess: true),
            initial.copyWith(
                isLoginTogglePosition: false,
                isPasswordValid: true,
                isEmailValid: true,
                isRegisterSuccess: false)
          ]);

  blocTest<LoginBloc, LoginState>(
      "Form submitted - invalid fields fixed, login success",
      build: () => loginBloc,
      seed: () => initial.copyWith(isEmailValid: false, isPasswordValid: false),
      act: (bloc) => bloc.add(FormSubmitted(validEmail, validPassword)),
      expect: () => <LoginState>[
            initial.copyWith(isPasswordValid: true, isEmailValid: true),
            initial.copyWith(
                isPasswordValid: true, isEmailValid: true, isLoginSuccess: true)
          ]);

  blocTest<LoginBloc, LoginState>("Switch toggled - success",
      build: () => loginBloc,
      act: (bloc) => bloc.add(SwitchToggled(false)),
      expect: () =>
          <LoginState>[initial.copyWith(isLoginTogglePosition: false)]);

  blocTest<LoginBloc, LoginState>("Switch toggled - nothing happens",
      build: () => loginBloc,
      seed: () => initial.copyWith(isRegisterSuccess: true),
      act: (bloc) => bloc.add(SwitchToggled(false)),
      expect: () => <LoginState>[]);

  tearDown(() {
    loginBloc.close();
  });
}

class LoginUseCaseMock implements LoginUserUseCase {
  @override
  Future<void> execute(String email, String password) {
    return Future.value();
  }
}
