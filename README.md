# login_test

Flutter test assignment.

# Description
App opens to show login form if user has not authenticated yet.

## Logging in
If the user clicks on the login button then the user can sign in. All e-mails will do using the correct pattern for an e-mail. Validated using regex.
Password is validated using regex. Must be 8 digits long and contain letters and numbers.
If you enter invalid data into the fields then errors are shown on each form field.
If correct data is entered then user is taken into the home screen area where their e-mail is shown.
In the home screen the user can click on the log out button on the app bar.
If they close the app after that then and come back they stay logged in.

## Registration
If the user does the flow described in the previous step and click on "Sign-up" then a snack bar is shown which lets them know of a succesful registration.
After 2 seconds the user is taken to the home screen, the same as with the Logging in flow.

## Forgot password
Pressing this button just shows a fun snackbar explaining how you can log in.

# App details:
## Technical implementation
1. App localizations used for texts.
2. Views built using regular dart code and material library imports.
3. Views are controlled by blocs. Bloc is my go to state management tool as it allows for unidirectional data flow and makes logging everything that is happening in the app very simple by using a BlocObserver.
4. Bloc long going operations are done in use case classes.
5. Use cases use whatever means necessary to get their jobs done. In this project they use a user repository.
6. The user repository uses the shared_preferences library to store data.
7. Use cases or repositories come from their respective factories.
8. Authentication bloc controls the state of the app. Navigation is not needed as it can get things done just by switching widgets.

## Tests
1. Blocs are tested.
2. Provided a widget test for the LoginLayout.
Hope its enough to show that testing is doable.