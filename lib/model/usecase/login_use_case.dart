import 'package:login_test/model/repository/user_repository.dart';

class LoginUserUseCase {
  final UserRepository _userRepository;

  LoginUserUseCase(this._userRepository);

  Future<void> execute(String email, String password) async {
    return _userRepository.login(email, password);
  }
}
