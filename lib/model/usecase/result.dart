abstract class Result {}

class Success<T> extends Result {
  final T t;

  Success(this.t);
}

class Failure<E> extends Result {
  final E e;

  Failure(this.e);
}
