import 'package:login_test/model/repository/user_repository.dart';

class LogoutUsecase {
  final UserRepository _userRepository;

  LogoutUsecase(this._userRepository);

  Future<void> execute() async{
    return _userRepository.logout();
  }
}
