import 'package:login_test/model/repository/repository_factory.dart';
import 'package:login_test/model/usecase/get_user_use_case.dart';
import 'package:login_test/model/usecase/login_use_case.dart';
import 'package:login_test/model/usecase/logout_use_case.dart';

class UsecaseFactory {
  final RepositoryFactory _repositoryFactory;

  UsecaseFactory(this._repositoryFactory);

  LoginUserUseCase getLoginUsecase() {
    return LoginUserUseCase(_repositoryFactory.getUserRepository());
  }

  GetUserUsecase getUserUsecase() {
    return GetUserUsecase(_repositoryFactory.getUserRepository());
  }

  LogoutUsecase getLogoutUsecase() {
    return LogoutUsecase(_repositoryFactory.getUserRepository());
  }
}
