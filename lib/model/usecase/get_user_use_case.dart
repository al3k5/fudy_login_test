import 'package:login_test/model/repository/user_repository.dart';

import 'result.dart';

class GetUserUsecase {
  final UserRepository _userRepository;

  GetUserUsecase(this._userRepository);

  Result execute() {
    try {
      return Success(_userRepository.getUser());
    } catch (e){
      return Failure(e);
    }
  }
}
