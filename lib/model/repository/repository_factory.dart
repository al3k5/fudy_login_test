import 'package:login_test/model/repository/user_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RepositoryFactory {
  final UserRepository _userRepository;

  RepositoryFactory(SharedPreferences sharedPreferences)
      : _userRepository = UserRepository(sharedPreferences);

  UserRepository getUserRepository() {
    return _userRepository;
  }
}
