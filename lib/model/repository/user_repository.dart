import 'package:login_test/constants/shared_pref_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../User.dart';

class UserRepository {
  final SharedPreferences _sharedPreferences;

  UserRepository(this._sharedPreferences);

  Future<void> login(String email, String password) async {
    await _sharedPreferences.setString(prefsEmail, email);
    await _sharedPreferences.setString(prefsPassword, password);
  }

  Future<void> logout() async {
    await _sharedPreferences.remove(prefsEmail);
    await _sharedPreferences.remove(prefsPassword);
  }

  User getUser() {
    if (_sharedPreferences.containsKey(prefsEmail)) {
      return User(email: _sharedPreferences.getString(prefsEmail)!);
    }
    throw Exception("No user logged in!");
  }
}
