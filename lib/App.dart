import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:login_test/authentication/authentication_bloc.dart';
import 'package:login_test/authentication/authentication_event.dart';
import 'package:login_test/authentication/authentication_state.dart';
import 'package:login_test/home/home_screen.dart';
import 'package:login_test/login/login_bloc.dart';
import 'package:login_test/login/login_screen.dart';
import 'package:login_test/model/repository/repository_factory.dart';
import 'package:login_test/model/usecase/use_case_factory.dart';
import 'package:login_test/splash/splash_screen.dart';
import 'package:login_test/theme/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  UsecaseFactory? _usecaseFactory;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: theme,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      home: FutureBuilder<void>(
          future: initializeDependencies(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return BlocProvider<AuthenticationBloc>(
                create: (context) {
                  return AuthenticationBloc(_usecaseFactory!.getUserUsecase(),
                      _usecaseFactory!.getLogoutUsecase())
                    ..add(Initial());
                },
                child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
                  builder: (context, state) {
                    if (state is Unauthenticated) {
                      return BlocProvider<LoginBloc>(
                        create: (context) {
                          return LoginBloc(
                              loginUserUseCase:
                                  _usecaseFactory!.getLoginUsecase());
                        },
                        child: LoginScreen(),
                      );
                    } else if (state is Authenticated) {
                      return HomeScreen();
                    } else {
                      return SplashScreen();
                    }
                  },
                ),
              );
            } else {
              return SplashScreen();
            }
          }),
    );
  }

  Future<void> initializeDependencies() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    RepositoryFactory repositoryFactory = RepositoryFactory(preferences);
    _usecaseFactory = UsecaseFactory(repositoryFactory);
  }
}
