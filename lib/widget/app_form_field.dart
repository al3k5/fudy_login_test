import 'package:flutter/material.dart';
import 'package:login_test/constants/ui_constants.dart';

class AppFormField extends StatefulWidget {
  final TextEditingController controller;
  final String hintText;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final bool obscureText;
  final String? errorText;
  final Function(String value)? onTextFieldSubmitted;

  const AppFormField(
      {Key? key,
      required this.controller,
      this.hintText = "",
      this.keyboardType = TextInputType.text,
      this.textInputAction = TextInputAction.next,
      this.obscureText = false,
      this.errorText,
      this.onTextFieldSubmitted})
      : super(key: key);

  @override
  _AppFormFieldState createState() => _AppFormFieldState();
}

class _AppFormFieldState extends State<AppFormField> {
  double _border = 3;

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return TextFormField(
      controller: widget.controller,
      keyboardType: widget.keyboardType,
      textInputAction: widget.textInputAction,
      obscureText: widget.obscureText,
      style: themeData.textTheme.bodyText1
          ?.copyWith(color: themeData.primaryColor),
      decoration: InputDecoration(
        hintText: widget.hintText,
        hintStyle: themeData.textTheme.bodyText1
            ?.copyWith(color: themeData.primaryColorDark),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: themeData.primaryColorDark,
            width: _border,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(appElementsBorderRadius),
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: themeData.errorColor,
            width: _border,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(appElementsBorderRadius),
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: themeData.errorColor,
            width: _border,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(appElementsBorderRadius),
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: themeData.primaryColor,
            width: _border,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(appElementsBorderRadius),
          ),
        ),
        contentPadding: EdgeInsets.all(roundElementPadding),
      ),
      autovalidateMode: AutovalidateMode.always,
      validator: (_) {
        return widget.errorText;
      },
      onFieldSubmitted: widget.onTextFieldSubmitted,
    );
  }
}
