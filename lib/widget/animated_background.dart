import 'package:flutter/material.dart';
import 'package:login_test/constants/ui_constants.dart';

import 'helper/background_painter.dart';

class AnimatedBackground extends StatefulWidget {
  final bool reverse;
  final Widget child;

  AnimatedBackground({Key? key, required this.reverse, required this.child})
      : super(key: key);

  @override
  _AnimatedBackgroundState createState() => _AnimatedBackgroundState();
}

class _AnimatedBackgroundState extends State<AnimatedBackground>
    with SingleTickerProviderStateMixin {
  late final AnimationController _animationController = AnimationController(
    vsync: this,
    duration: Duration(milliseconds: longAnimationLength),
  );

  @override
  Widget build(BuildContext context) {
    if (widget.reverse) {
      _animationController.reverse();
    } else {
      _animationController.forward();
    }
    return CustomPaint(
      size: MediaQuery.of(context).size,
      painter: BackgroundPainter(
          color: Theme.of(context).canvasColor,
          animation: _animationController.view),
      child: widget.child,
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
