import 'package:flutter/material.dart';

class BackgroundPainter extends CustomPainter {
  final Animation<double> _widthOffset1;
  final Animation<double> _widthOffset2;
  final Color color;

  BackgroundPainter({required this.color, required Animation<double> animation})
      : _widthOffset1 = Tween(begin: 0.0, end: 0.4).animate(animation),
        _widthOffset2 = Tween(begin: 0.0, end: 0.5).animate(animation),
        super(repaint: animation);

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = color
      ..strokeWidth = 10.0;

    var path = Path();

    path.moveTo(0, size.height * 0.33);

    path.quadraticBezierTo(
        size.width * (0.05 + _widthOffset1.value),
        size.height * 0.2,
        size.width * (0.3 + _widthOffset1.value),
        size.height * 0.2);
    path.quadraticBezierTo(size.width * (0.45 + _widthOffset2.value),
        size.height * 0.2, size.width, size.height * 0.32);

    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(BackgroundPainter old) {
    return !_widthOffset1.isCompleted && !_widthOffset2.isCompleted;
  }
}
