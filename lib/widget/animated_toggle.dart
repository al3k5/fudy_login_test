import 'package:flutter/material.dart';
import 'package:login_test/constants/ui_constants.dart';

class AnimatedToggle extends StatefulWidget {
  final bool isInInitialPosition;
  final String firstButtonText;
  final String secondButtonText;
  final VoidCallback onFirstButtonToggled;
  final VoidCallback onSecondButtonToggled;
  final VoidCallback onFirstButtonConfirmed;
  final VoidCallback onSecondButtonConfirmed;

  AnimatedToggle(
      {Key? key,
      this.isInInitialPosition = true,
      required this.firstButtonText,
      required this.secondButtonText,
      required this.onFirstButtonToggled,
      required this.onSecondButtonToggled,
      required this.onFirstButtonConfirmed,
      required this.onSecondButtonConfirmed})
      : super(key: key);

  @override
  _AnimatedToggleState createState() => _AnimatedToggleState();
}

class _AnimatedToggleState extends State<AnimatedToggle> {
  bool _isInitialPosition = true;

  @override
  void initState() {
    super.initState();
    _isInitialPosition = widget.isInInitialPosition;
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return LayoutBuilder(
      builder: (builderContext, constraints) {
        return Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topLeft,
              child: InkWell(
                onTap: () {
                  if (_isInitialPosition) {
                    widget.onSecondButtonToggled();
                  } else {
                    widget.onFirstButtonToggled();
                  }
                  setState(() {
                    _isInitialPosition = !_isInitialPosition;
                  });
                },
                child: Container(
                  width: constraints.maxWidth,
                  padding: EdgeInsets.symmetric(vertical: roundElementPadding),
                  decoration: ShapeDecoration(
                    color: themeData.shadowColor,
                    shape: RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.circular(appElementsBorderRadius)),
                  ),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: constraints.maxWidth * 0.0625),
                          child: Text(
                            widget.firstButtonText,
                            style: themeData.textTheme.bodyText1
                                ?.copyWith(color: themeData.disabledColor),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: constraints.maxWidth * 0.0625),
                          child: Text(
                            widget.secondButtonText,
                            style: themeData.textTheme.bodyText1
                                ?.copyWith(color: themeData.disabledColor),
                          ),
                        )
                      ]),
                ),
              ),
            ),
            AnimatedAlign(
              duration: const Duration(milliseconds: shortAnimationLength),
              curve: Curves.decelerate,
              alignment:
                  _isInitialPosition ? Alignment.topLeft : Alignment.topRight,
              child: InkWell(
                onTap: () {
                  if (_isInitialPosition) {
                    widget.onFirstButtonConfirmed();
                  } else {
                    widget.onSecondButtonConfirmed();
                  }
                },
                child: Container(
                  width: constraints.maxWidth * 0.55,
                  padding: EdgeInsets.symmetric(vertical: roundElementPadding),
                  decoration: ShapeDecoration(
                    color: themeData.primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(appElementsBorderRadius),
                    ),
                  ),
                  child: Text(
                    _isInitialPosition
                        ? widget.firstButtonText
                        : widget.secondButtonText, // : widget.values[1],
                    style: themeData.textTheme.bodyText1
                        ?.copyWith(color: themeData.accentColor),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
