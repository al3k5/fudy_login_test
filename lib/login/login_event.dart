import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {}

class SwitchToggled extends LoginEvent {
  final bool isInInitialPosition;

  SwitchToggled(this.isInInitialPosition);

  @override
  List<Object> get props => [isInInitialPosition];
}

class LoginClicked extends LoginEvent {
  final String email;
  final String password;

  LoginClicked(this.email, this.password);

  @override
  List<Object> get props => [email, password];
}

class RegisterClicked extends LoginEvent {
  final String email;
  final String password;

  RegisterClicked(this.email, this.password);

  @override
  List<Object> get props => [email, password];
}

class FormSubmitted extends LoginEvent {
  final String email;
  final String password;

  FormSubmitted(this.email, this.password);

  @override
  List<Object?> get props => [email, password];
}
