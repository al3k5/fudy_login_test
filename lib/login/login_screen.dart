import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:login_test/authentication/authentication_bloc.dart';
import 'package:login_test/authentication/authentication_event.dart';
import 'package:login_test/login/login_bloc.dart';
import 'package:login_test/login/login_layout.dart';

import 'login_state.dart';

class LoginScreen extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginBloc, LoginState>(listener: (context, state) {
      if (state.isRegisterSuccess) {
        ScaffoldMessengerState scaffoldMessenger =
            ScaffoldMessenger.of(context);
        scaffoldMessenger.showSnackBar(SnackBar(
            backgroundColor: Theme.of(context).splashColor,
            content: Text(AppLocalizations.of(context)!.loginRegisterSuccess)));
      }
      if (state.isLoginSuccess) {
        BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
      }
    }, builder: (context, state) {
      return LoginLayout(
        state: state,
        emailController: _emailController,
        passwordController: _passwordController,
      );
    });
  }
}
