import 'package:bloc/bloc.dart';
import 'package:login_test/login/login_event.dart';
import 'package:login_test/login/login_state.dart';
import 'package:login_test/model/usecase/login_use_case.dart';
import 'package:login_test/model/validators.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginUserUseCase loginUserUseCase;

  LoginBloc({required this.loginUserUseCase}) : super(LoginState());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is SwitchToggled) {
      if (!state.isRegisterSuccess) {
        yield state.copyWith(isLoginTogglePosition: event.isInInitialPosition);
      }
    } else if (event is LoginClicked) {
      if (!state.isRegisterSuccess) {
        final isEmailValid = Validators.isValidEmail(event.email);
        final isPasswordValid = Validators.isValidPassword(event.password);

        yield state.copyWith(
            isEmailValid: isEmailValid, isPasswordValid: isPasswordValid);
        if (isEmailValid && isPasswordValid) {
          await loginUserUseCase.execute(event.email, event.password);
          yield state.copyWith(isLoginSuccess: true);
        }
      }
    } else if (event is RegisterClicked) {
      if (!state.isRegisterSuccess) {
        final isEmailValid = Validators.isValidEmail(event.email);
        final isPasswordValid = Validators.isValidPassword(event.password);

        yield state.copyWith(
            isEmailValid: isEmailValid, isPasswordValid: isPasswordValid);
        if (isEmailValid && isPasswordValid) {
          yield state.copyWith(isRegisterSuccess: true);
          await Future.delayed(const Duration(seconds: 2));
          yield state.copyWith(isRegisterSuccess: false);
          add(LoginClicked(event.email, event.password));
        }
      }
    } else if (event is FormSubmitted) {
      if (state.isLoginTogglePosition) {
        add(LoginClicked(event.email, event.password));
      } else {
        add(RegisterClicked(event.email, event.password));
      }
    }
  }
}
