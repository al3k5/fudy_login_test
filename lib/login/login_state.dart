import 'package:equatable/equatable.dart';

class LoginState extends Equatable {
  final bool isEmailValid;
  final bool isPasswordValid;
  final bool isLoginTogglePosition;
  final bool isLoginSuccess;
  final bool isRegisterSuccess;

  const LoginState(
      {this.isEmailValid = true,
      this.isPasswordValid = true,
      this.isLoginTogglePosition = true,
      this.isLoginSuccess = false,
      this.isRegisterSuccess = false});

  LoginState copyWith(
      {bool? isEmailValid,
      bool? isPasswordValid,
      bool? isLoginTogglePosition,
      bool? isLoginSuccess,
      bool? isRegisterSuccess}) {
    return LoginState(
        isEmailValid: isEmailValid ?? this.isEmailValid,
        isPasswordValid: isPasswordValid ?? this.isPasswordValid,
        isLoginTogglePosition:
            isLoginTogglePosition ?? this.isLoginTogglePosition,
        isLoginSuccess: isLoginSuccess ?? this.isLoginSuccess,
        isRegisterSuccess: isRegisterSuccess ?? this.isRegisterSuccess);
  }

  @override
  List<Object> get props => [
        isEmailValid,
        isPasswordValid,
        isLoginTogglePosition,
        isLoginSuccess,
        isRegisterSuccess
      ];

  @override
  String toString() {
    return 'LoginState{isEmailValid: $isEmailValid, isPasswordValid:'
        ' $isPasswordValid, isLoginTogglePosition: $isLoginTogglePosition, '
        'isLoginSuccess: $isLoginSuccess, isRegisterSuccess: '
        '$isRegisterSuccess}';
  }
}
