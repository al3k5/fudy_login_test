import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login_test/constants/ui_constants.dart';
import 'package:login_test/login/login_state.dart';
import 'package:login_test/widget/animated_background.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:login_test/widget/animated_toggle.dart';
import 'package:login_test/widget/app_form_field.dart';

import 'login_bloc.dart';
import 'login_event.dart';

class LoginLayout extends StatelessWidget {
  final LoginState state;

  final TextEditingController emailController;
  final TextEditingController passwordController;
  final double horizontalPadding = 32.0;

  LoginLayout(
      {Key? key,
      required this.state,
      required this.emailController,
      required this.passwordController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        body: SingleChildScrollView(
      child: AnimatedBackground(
        reverse: !state.isLoginTogglePosition,
        child: screenSize.height > screenSize.width
            ? _portraitMode(context, screenSize, state)
            : _landscapeMode(context, screenSize, state),
      ),
    ));
  }

  Widget _portraitMode(
      BuildContext context, Size screenSize, LoginState state) {
    var logoPaddingTop = 100.0;
    var verticalPaddingFields = 32.0;
    var topPaddingForgotPassword = 72.0;
    ThemeData themeData = Theme.of(context);
    return Container(
      height: screenSize.height,
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
      child: Column(children: [
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: EdgeInsets.only(top: logoPaddingTop),
            child: _logoText(context, themeData),
          ),
        ),
        Spacer(),
        _loginFormField(context, state),
        Padding(
          padding: EdgeInsets.only(top: verticalPaddingFields),
          child: _passwordFormField(context, state),
        ),
        Padding(
          padding: EdgeInsets.only(top: verticalPaddingFields),
          child: _animatedToggle(context, state),
        ),
        Padding(
          padding: EdgeInsets.only(top: topPaddingForgotPassword),
          child: _forgotPasswordButton(context, themeData),
        ),
        Spacer(),
      ]),
    );
  }

  Widget _landscapeMode(
      BuildContext context, Size screenSize, LoginState state) {
    ThemeData themeData = Theme.of(context);
    var logoPaddingTop = 50.0;
    var formFieldsPadding = 12.0;
    var paddingVertical = screenSize.height * 0.05;
    return Container(
      height: screenSize.height,
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
      child: Column(children: [
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: EdgeInsets.only(top: logoPaddingTop),
            child: _logoText(context, themeData),
          ),
        ),
        Spacer(),
        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: formFieldsPadding),
              child: _loginFormField(context, state),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: formFieldsPadding),
              child: _passwordFormField(context, state),
            ),
          ),
        ]),
        Padding(
          padding: EdgeInsets.only(top: paddingVertical),
          child: _animatedToggle(context, state),
        ),
        Padding(
          padding: EdgeInsets.only(top: paddingVertical),
          child: _forgotPasswordButton(context, themeData),
        ),
        Spacer(),
      ]),
    );
  }

  Widget _logoText(BuildContext context, ThemeData themeData) {
    return Text(
      AppLocalizations.of(context)!.loginBrandLogo,
      style:
          themeData.textTheme.headline5?.copyWith(color: themeData.accentColor),
    );
  }

  Widget _loginFormField(BuildContext context, LoginState state) {
    return AppFormField(
        key: loginLoginEmailKey,
        controller: emailController,
        hintText: AppLocalizations.of(context)!.loginUsername,
        keyboardType: TextInputType.emailAddress,
        errorText: state.isEmailValid
            ? null
            : AppLocalizations.of(context)!.loginEmailError);
  }

  Widget _passwordFormField(BuildContext context, LoginState state) {
    return AppFormField(
      key: loginPasswordKey,
      controller: passwordController,
      hintText: AppLocalizations.of(context)!.loginPassword,
      keyboardType: TextInputType.text,
      obscureText: true,
      textInputAction: TextInputAction.go,
      errorText: state.isPasswordValid
          ? null
          : AppLocalizations.of(context)!.loginPasswordError,
      onTextFieldSubmitted: (_) {
        BlocProvider.of<LoginBloc>(context)
            .add(FormSubmitted(emailController.text, passwordController.text));
      },
    );
  }

  Widget _animatedToggle(BuildContext context, LoginState state) {
    return AnimatedToggle(
      isInInitialPosition: state.isLoginTogglePosition,
      firstButtonText: AppLocalizations.of(context)!.loginLogin,
      secondButtonText: AppLocalizations.of(context)!.loginSignup,
      onFirstButtonToggled: () {
        BlocProvider.of<LoginBloc>(context).add(SwitchToggled(true));
      },
      onSecondButtonToggled: () {
        BlocProvider.of<LoginBloc>(context).add(SwitchToggled(false));
      },
      onFirstButtonConfirmed: () {
        BlocProvider.of<LoginBloc>(context)
            .add(LoginClicked(emailController.text, passwordController.text));
      },
      onSecondButtonConfirmed: () {
        BlocProvider.of<LoginBloc>(context).add(
            RegisterClicked(emailController.text, passwordController.text));
      },
    );
  }

  Widget _forgotPasswordButton(BuildContext context, ThemeData themeData) {
    return TextButton(
        onPressed: () {
          ScaffoldMessengerState scaffoldMessenger =
              ScaffoldMessenger.of(context);
          scaffoldMessenger.showSnackBar(SnackBar(
              backgroundColor: themeData.primaryColor,
              content: Text(
                  AppLocalizations.of(context)!.loginAlmostAnyCredentials)));
        },
        child: Text(
          AppLocalizations.of(context)!.loginForgotPassword,
          style: themeData.textTheme.bodyText1
              ?.copyWith(color: themeData.primaryColorDark),
        ));
  }
}
