import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:login_test/App.dart';
import 'package:login_test/model/bloc_observer.dart';

void main() {
  Bloc.observer = MyBlocObserver();
  runApp(App());
}
