import 'dart:ui';

import 'package:flutter/material.dart';

Color darkPrimaryColor = Color.fromRGBO(220, 103, 94, 1);
Color darkPrimaryColorDark = Color.fromRGBO(143, 75, 76, 1);
Color darkScaffoldColor = Color.fromRGBO(30, 33, 52, 1);
Color darkCanvasColor = Color.fromRGBO(14, 28, 48, 1);
Color darkAccentColor = Colors.white;
Color darkShadowColor = Color.fromRGBO(37, 51, 68, 1);
Color darkDisabledColor = Color.fromRGBO(68, 81, 96, 1);
Color darkSplashColor = Colors.green;
