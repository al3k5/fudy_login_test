import 'package:flutter/material.dart';
import 'package:login_test/theme/color.dart';

// TODO Add light/dark theme and change colors
ThemeData theme = ThemeData.light().copyWith(
    scaffoldBackgroundColor: darkScaffoldColor,
    canvasColor: darkCanvasColor,
    primaryColor: darkPrimaryColor,
    primaryColorDark: darkPrimaryColorDark,
    accentColor: darkAccentColor,
    disabledColor: darkDisabledColor,
    shadowColor: darkShadowColor,
    splashColor: darkSplashColor);
