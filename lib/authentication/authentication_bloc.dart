import 'package:bloc/bloc.dart';
import 'package:login_test/authentication/authentication_event.dart';
import 'package:login_test/authentication/authentication_state.dart';
import 'package:login_test/model/usecase/result.dart';
import 'package:login_test/model/usecase/get_user_use_case.dart';
import 'package:login_test/model/usecase/logout_use_case.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final GetUserUsecase getUserUsecase;
  final LogoutUsecase logoutUsecase;

  AuthenticationBloc(this.getUserUsecase, this.logoutUsecase)
      : super(Uninitialized());

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is Initial) {
      Result userResult = getUserUsecase.execute();
      if (userResult is Success) {
        yield Authenticated(userResult.t);
      } else {
        yield Unauthenticated();
      }
    } else if (event is LoggedIn) {
      Result userResult = getUserUsecase.execute();
      if (userResult is Success) {
        yield Authenticated(userResult.t);
      } else {
        yield Unauthenticated();
      }
    } else if (event is LoggedOut) {
      await logoutUsecase.execute();
      yield Unauthenticated();
    }
  }
}
