import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:login_test/authentication/authentication_bloc.dart';
import 'package:login_test/authentication/authentication_event.dart';
import 'package:login_test/authentication/authentication_state.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
              icon: const Icon(
                Icons.exit_to_app,
              ),
              onPressed: () {
                BlocProvider.of<AuthenticationBloc>(context).add(
                  LoggedOut(),
                );
              },
            )
          ],
        ),
        body: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, state) {
            if (state is Authenticated) {
              return Center(
                child: Text(
                  "${AppLocalizations.of(context)!.homeWelcome} ${state.user.email}!",
                  style: themeData.textTheme.bodyText1?.copyWith(color: themeData.primaryColor),
                ),
              );
            } else {
              return Container();
            }
          },
        ));
  }
}
