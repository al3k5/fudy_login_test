import 'package:flutter/material.dart';

const double roundElementPadding = 20;
const double appElementsBorderRadius = 40;

const int shortAnimationLength = 250;
const int longAnimationLength = 500;

const Key loginLoginEmailKey = Key("email");
const Key loginPasswordKey = Key("password");
